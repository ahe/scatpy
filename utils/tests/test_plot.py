import pytest
from utils import plot, folders, io
import numpy.testing as npt
import os
import pyfar as pf
from pyfar.testing.plot_utils import create_figure, save_and_compare

"""
Testing plots is difficult, as matplotlib does not create the exact same
figures on different systems (e.g. single pixels vary).
Therefore, this file serves several purposes:
1. The usual call of pytest, which only checks, if the functions do not raise
errors.
2. Creating baseline figures. If the global parameter `create_baseline` is
set to True, figures are created in the corresponding folder. These need to be
updated and manually inspected and if the plot look changed.
3. Comparing the created images to the baseline images by setting the global
parameter `compare_output`. This function should only be activated if intended.
IMPORTANT: IN THE REPOSITORY, BOTH `CREATE_BASELINE` AND `COMPARE_OUTPUT` NEED
TO BE SET TO FALSE, SO THE TRAVIS-CI CHECKS DO NOT FAIL.
"""
# global parameters -----------------------------------------------------------
create_baseline = False

# file type used for saving the plots
file_type = "png"

# if true, the plots will be compared to the baseline and an error is raised
# if there are any differences. In any case, differences are written to
# output_path as images
compare_output = False

# path handling
base_path = os.path.join(folders.test_path(), 'test_plot_data')
baseline_path = os.path.join(base_path, 'baseline')
output_path = os.path.join(base_path, 'output')

if not os.path.isdir(base_path):
    os.mkdir(base_path)
if not os.path.isdir(baseline_path):
    os.mkdir(baseline_path)
if not os.path.isdir(output_path):
    os.mkdir(output_path)

# remove old output files
for file in os.listdir(output_path):
    os.remove(os.path.join(output_path, file))


@pytest.mark.parametrize("input, desired",  [
    (5, 1.96850393701),
    (-5, -1.96850393701),
    ((5, 5), (1.96850393701, 1.96850393701)),
    ])
def test_cm2inch(input, desired):
    actual = plot.cm2inch(input)
    npt.assert_allclose(actual, desired)


@pytest.mark.parametrize('function', [
    (plot.scattering_polar)])
def test_line_plots(function):
    """Test all line plots with default arguments and hold functionality."""
    print("Testing: scattering_polar")
    root = os.path.join(
        folders.test_path(), 'test_io_data', 'ScatteringPattern')
    file_path_sample = 'point_120_1.ScatteringPattern.far'
    file_path_ref = 'point_120_reference_1_3.0cm.ScatteringPattern.far'

    data, meta_pattern = io.read_comsol_set_with_meta(
        os.path.join(root, file_path_sample))
    data_ref, meta_pattern_ref = io.read_comsol_set_with_meta(
        os.path.join(root, file_path_ref))
    data_ref = pf.FrequencyData(
        data_ref.freq[..., :len(data.frequencies)], data.frequencies)
    phi = 180
    theta = 80
    coords_inc = meta_pattern.incident_coords
    coords_mic = meta_pattern.mics_coords
    frequency = 2000

    # initial plot
    filename = 'scattering_polar_default'
    create_figure()
    plot.scattering_polar(
        data, data_ref, coords_inc, coords_mic, phi, theta, frequency)
    save_and_compare(create_baseline, baseline_path, output_path, filename,
                     file_type, compare_output)
