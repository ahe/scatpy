import numpy as np
import pyfar as pf
import os.path
from utils import spatial
from utils import folders
import numpy.testing as npt
import pytest


def test_shift_data_coords():
    data_raw = np.arange(0, 8).reshape(4, 2, 1)
    data = pf.FrequencyData(np.tile(data_raw, 2), [100, 200])

    azimuth = np.array([[0, 90, 180, 270], [0, 90, 180, 270]]).transpose()
    elevation = np.array([[20, 20, 20, 20], [40, 40, 40, 40]]).transpose()
    coords = pf.Coordinates(
        azimuth, elevation, np.ones((4, 2)), 'sph', unit='deg')
    audio = spatial.shift_data_coords(data, coords, 90)
    npt.assert_almost_equal(
        audio.freq[..., 0], np.array([[6., 7.], [0., 1.], [2., 3.], [4., 5.]]))
    npt.assert_almost_equal(
        audio.freq[..., 1], np.array([[6., 7.], [0., 1.], [2., 3.], [4., 5.]]))


def test_shift_data_coords_for_and_back():
    file_name = 'plane_test.csv'
    path = os.path.join(folders.test_path(), 'test_io_data', file_name)
    data, coords_mic = pf.io.read_comsol(path)
    _, _, parameters, _, _ = pf.io.read_comsol_header(path)
    data.freq = np.squeeze(data.freq)
    all_az = np.linspace(0, 2*np.pi, 37)
    all_az[-1] = 0
    coords_out = spatial.angles2coords(
        all_az, np.linspace(0, np.pi/2, 10), 100)
    data = spatial.reshape_to_az_by_el(data, coords_mic, coords_out)
    coords_mic = coords_out

    shifted = spatial.shift_data_coords(data, coords_mic, 60)
    back_shifted = spatial.shift_data_coords(shifted, coords_mic, -60)
    npt.assert_array_almost_equal(back_shifted.freq, data.freq)


@pytest.mark.parametrize(
    'file_name', [
        ('plane_test_reference.csv')
    ])
def test_reshape_spherical_coordinates(file_name):
    path = os.path.join(folders.test_path(), 'test_io_data', file_name)
    data, coords = pf.io.read_comsol(path)
    all_az = np.linspace(0, 2*np.pi, 37)
    all_az[-1] = 0
    coords_out = spatial.angles2coords(
        all_az, np.linspace(0, np.pi/2, 10), 100)
    data_actual = spatial.reshape_to_az_by_el(data, coords, coords_out)
    coords_actual = coords_out
    npt.assert_allclose(data_actual.cshape[:1], coords_actual.cshape[:1])
    for iaz in range(coords_actual.cshape[0]):
        for iel in range(coords_actual.cshape[1]):
            xyz = coords_actual[iaz, iel].get_cart()
            idx_, _ = coords.find_nearest_k(
                xyz[..., 0], xyz[..., 1], xyz[..., 2])
            npt.assert_allclose(data[idx_].freq, data_actual[iaz, iel].freq)


def test_angles2coords_deg():
    azimuth = np.arange(0, 360+10, 360/10)
    elevation = np.arange(0, 90+10, 90/10)
    coords = spatial.angles2coords(azimuth, elevation, unit='deg')
    for i in range(coords.cshape[1]):
        npt.assert_allclose(coords.get_sph()[:, i, 0], azimuth/180*np.pi)
    for i in range(coords.cshape[0]):
        npt.assert_allclose(coords.get_sph()[i, :, 1], elevation/180*np.pi)


def test_angles2coords_rad():
    azimuth = np.arange(0, 360+10, 360/10)
    elevation = np.arange(0, 90+10, 90/10)
    azimuth *= np.pi/180.
    elevation *= np.pi/180.
    coords = spatial.angles2coords(azimuth, elevation)
    for i in range(coords.cshape[1]):
        npt.assert_allclose(coords.get_sph()[:, i, 0], azimuth)
    for i in range(coords.cshape[0]):
        npt.assert_allclose(coords.get_sph()[i, :, 1], elevation)


def test_apply_symmetry_circular():
    file_name = 'plane_test_reference.csv'
    path = folders.test_path()
    azimuth_inc = np.arange(0, 360, 30)
    data, coords_mic = pf.io.read_comsol(
        os.path.join(path, 'test_io_data', file_name))
    data.freq = np.squeeze(data.freq)
    _, _, parameters, _, _ = pf.io.read_comsol_header(
        os.path.join(path, 'test_io_data', file_name))
    incident_coords = spatial.angles2coords(
        0, parameters['theta'], 1.2)
    all_az = np.linspace(0, 2*np.pi, 37)
    all_az[-1] = 0
    coords_out = spatial.angles2coords(
        all_az, np.linspace(0, np.pi/2, 10), 100)
    data = spatial.reshape_to_az_by_el(data, coords_mic, coords_out)
    coords_mic = coords_out
    data_out, coords_out = spatial.apply_symmetry_circular(
        data, coords_mic, incident_coords, azimuth_inc, 2)
    assert data_out.cshape[0] == coords_out.cshape[0]
    assert data_out.cshape[1] == coords_out.cshape[1]
    assert data_out.cshape[2] == coords_mic.cshape[0]
    assert data_out.cshape[3] == coords_mic.cshape[1]
    npt.assert_array_almost_equal(
        coords_out.get_sph(unit='deg')[:, 0, 0], azimuth_inc)
    for idx, az in enumerate(azimuth_inc):
        shifted = spatial.shift_data_coords(data, coords_mic, float(az))
        shifted.freq = shifted.freq[np.newaxis, ...]
        shifted.freq = np.moveaxis(shifted.freq, 2+1, 1)
        npt.assert_array_almost_equal(
            shifted.freq[0, ...], data_out.freq[idx, ...])


def test_apply_symmetry_mirror():
    file_name = 'plane_test.csv'
    path = os.path.join(folders.test_path(), 'test_io_data', file_name)
    data, coords_mic = pf.io.read_comsol(path)
    _, _, parameters, _, _ = pf.io.read_comsol_header(path)
    data.freq = np.squeeze(data.freq)
    incident_coords = spatial.angles2coords(
        parameters['phi'], parameters['theta'], 1.2)
    all_az = np.linspace(0, 2*np.pi, 37)
    all_az[-1] = 0
    coords_out = spatial.angles2coords(
        all_az, np.linspace(0, np.pi/2, 10), 100)
    data = spatial.reshape_to_az_by_el(data, coords_mic, coords_out)
    coords_mic = coords_out
    data_out, coords_out = spatial.apply_symmetry_mirror(
        data, coords_mic, incident_coords, 2)
    data_out_2, coords_out_2 = spatial.apply_symmetry_mirror(
        data_out, coords_mic, coords_out, 2)
    assert data_out.cshape[0] == data.cshape[0]
    assert data_out.cshape[1] == data.cshape[1]
    assert data_out.cshape[2] == data.cshape[2]*2-1
    assert data_out.cshape[3] == data.cshape[3]
    shifted = spatial.shift_data_coords(data, coords_mic, 60)
    npt.assert_array_almost_equal(
        shifted.freq[:, :, 2, :, :], data_out.freq[:, :, 4, :, :])
    shifted = spatial.shift_data_coords(data, coords_mic, 120)
    npt.assert_array_almost_equal(
        shifted.freq[:, :, 1, :, :], data_out.freq[:, :, 5, :, :])
    shifted = spatial.shift_data_coords(data, coords_mic, 180)
    npt.assert_array_almost_equal(
        shifted.freq[:, :, 0, :, :], data_out.freq[:, :, 6, :, :])
    assert coords_out.cshape[0] == data_out.cshape[2]
    azimuths = coords_out.get_sph()[:, 1, 0]
    npt.assert_array_almost_equal(
        azimuths, np.arange(0, 180+30, 30)/180*np.pi, decimal=5)
    azimuths = coords_out_2.get_sph()[:, 1, 0]
    npt.assert_array_almost_equal(
        azimuths, np.arange(0, 360, 30)/180*np.pi, decimal=5)


def test_apply_symmetry_circular_input_doesnt_change(coords_10_deg):
    data_in = np.zeros((37, 10, 2, 3), dtype=complex)
    data_in[0, 0, 0, 0] = 1
    data = pf.FrequencyData(data_in, [1, 2, 3])
    coords_mic = coords_10_deg
    coords_inc = pf.Coordinates(0, [30, 40], 1, 'sph', unit='deg')
    azimuth_out = np.linspace(0, 360, 13)
    data_cp = data.copy()
    data_out, coords = spatial.apply_symmetry_circular(
        data, coords_mic, coords_inc, azimuth_out, 2)
    assert data_cp == data
    assert data_cp[:, :, 0] == data_out[0, 0, :, :]
    assert data[:, :, 0] == data_out[0, 0, :, :]
