import numpy as np
from utils import io, spatial, folders, meta
import os.path
import pyfar as pf
import pytest


def test_write_comsol_set_with_meta(scattering_pattern, tmpdir):
    file_name = 'plane_test.csv'
    path = os.path.join(folders.test_path(), 'test_io_data', file_name)
    data, coords_mic = pf.io.read_comsol(path)
    data.freq = np.squeeze(data.freq)
    all_az = np.linspace(0, 2*np.pi, 37)
    all_az[-1] = 0
    coords_out = spatial.angles2coords(
        all_az, np.linspace(0, np.pi/2, 10), 100)
    data = spatial.reshape_to_az_by_el(data, coords_mic, coords_out)
    coords_mic = coords_out

    # actual test
    full_path_far = io.write_comsol_set_with_meta(
        data, scattering_pattern, tmpdir)
    data_actual, meta_pattern_actual = io.read_comsol_set_with_meta(
        full_path_far)
    assert data == data_actual
    assert scattering_pattern == meta_pattern_actual


@pytest.mark.parametrize(
    'file_name', [
        'plane_test.csv',
        'plane_test_reference.csv',
    ])
def test_write_comsol_set_with_meta_diff(file_name, person, env, tmpdir):
    file_name = 'plane_test.csv'
    path = os.path.join(folders.test_path(), 'test_io_data', file_name)
    data, coords_mic = pf.io.read_comsol(path)
    _, _, parameters, _, _ = pf.io.read_comsol_header(path)
    data.freq = np.squeeze(data.freq)
    incident_coords = spatial.angles2coords(
        parameters['phi'], parameters['theta'], 1.2)

    all_az = np.linspace(0, 2*np.pi, 37)
    all_az[-1] = 0
    coords_out = spatial.angles2coords(
        all_az, np.linspace(0, np.pi/2, 10), 1)
    data = spatial.reshape_to_az_by_el(data, coords_mic, coords_out)
    coords_mic = coords_out
    scattering_pattern = meta.ScatteringPattern(
        name='test', creater=person, environment=env,
        frequencies=data.frequencies, is_reference=False,
        incident_coords=incident_coords, mics_coords=coords_mic)

    # actual test
    full_path_far = io.write_comsol_set_with_meta(
        data, scattering_pattern, tmpdir)
    data_actual, meta_pattern_actual = io.read_comsol_set_with_meta(
        full_path_far)
    assert data == data_actual
    assert scattering_pattern == meta_pattern_actual
