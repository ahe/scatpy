from datetime import date
import pytest
import utils.meta as meta
import numpy as np
import numpy.testing as npt


def test_sample_defaults():
    sample = meta.Sample(name='sine')
    assert sample.diameter_m == 0.8
    assert sample.surf_wavelength_m == 0.177/2.5
    assert sample.surf_height_m == 0.051/2.5
    assert sample.surf_reflection == 1.
    assert sample.body_hight_m == 0.03
    assert sample.body_reflection == 1.
    assert sample.id == 'sine'


def test_sample_error_reflection_factor():
    error_msg = 'Reflection factor must be between -1 and 1.'
    with pytest.warns(UserWarning, match=error_msg):
        meta.Sample(name='', body_reflection=-5)
    with pytest.warns(UserWarning, match=error_msg):
        meta.Sample(name='', body_reflection=5)
    with pytest.warns(UserWarning, match=error_msg):
        meta.Sample(name='', surf_reflection=-5)
    with pytest.warns(UserWarning, match=error_msg):
        meta.Sample(name='', surf_reflection=5)


def test_all_error_identifier(half_sphere):
    error_msg = 'Given id is wrong an will be overwritten.'
    with pytest.warns(UserWarning, match=error_msg):
        sample = meta.Sample(id='5', name='')
    with pytest.warns(UserWarning, match=error_msg):
        person = meta.Person(
            id='5', name='fjo', given_name='Fritz', family_name='Josef')
    with pytest.warns(UserWarning, match=error_msg):
        env = meta.ComsolBem(id='5', name='hi', sample=sample)
    with pytest.warns(UserWarning, match=error_msg):
        meta.ScatteringCoefficientRandom(
            id='5', name='Hi', environment=env, creater=person,
            frequencies=[100])
    with pytest.warns(UserWarning, match=error_msg):
        meta.ScatteringCoefficient(
            id='5', name='Hi', environment=env, creater=person,
            frequencies=[100, 200], incident_coords=half_sphere)
    with pytest.warns(UserWarning, match=error_msg):
        meta.ScatteringPattern(
            id='5', name='Hi', environment=env, creater=person,
            frequencies=[100, 200], incident_coords=half_sphere,
            is_reference=False, mics_coords=half_sphere)


@pytest.mark.parametrize("name, d, wavelen, h, surf_r, body_r",  [
    ('hallo', 0.8, 0.05, 0.005, 1, 1),
    ('hallo5-', 0.8, 0.05, 0.005, -1, -1),
    ('hallo', 0.4, 0.05, 0.005, -1, -1),
    ('hallo', 0.8, 0.05, 0.05, -1, -1),
    ])
def test_sample_identifier(
        name, d, wavelen, h, surf_r, body_r):
    sample = meta.Sample(
        name=name, diameter_m=d, surf_wavelength_m=wavelen,
        surf_height_m=h, surf_reflection=surf_r, body_reflection=body_r)
    assert sample.id == name


def test_environment_defaults(sample):
    env = meta.Environment(name='hi', sample=sample)
    assert env.name == 'hi'
    assert env.date == date.today()
    assert env.sample == sample


def test_person_init():
    person = meta.Person(name='fjo', given_name='Fritz', family_name='Josef')
    assert person.given_name == 'Fritz'
    assert person.family_name == 'Josef'
    assert person.id == 'fjo'
    assert person.affiliation == 'Institute for Hearing Technology and '\
        'Acoustics, RWTH Aachen University'


def test_comsol_bem_init(sample):
    person = meta.ComsolBem(name='hi', sample=sample)
    assert person.comsol_version == ''
    assert person.model_name == ''


def test_scattering_random_init(person, env):
    scat = meta.ScatteringCoefficientRandom(
        name='Hi', creater=person, environment=env, frequencies=[100, 200])
    assert scat.name == 'Hi'
    assert scat.creater == person
    assert scat.environment == env
    npt.assert_array_almost_equal(scat.frequencies, np.array([100, 200]))


def test_scattering_coefficient_init(env, person, half_sphere):
    scat = meta.ScatteringCoefficient(
        name='Hi', environment=env, creater=person, frequencies=[100, 200],
        incident_coords=half_sphere)
    assert scat.name == 'Hi'
    assert scat.creater == person
    assert scat.environment == env
    npt.assert_array_almost_equal(scat.frequencies, np.array([100, 200]))


def test_scattering_pattern_init(
        env, person, incident_angles_rad, mics_angles_rad, half_sphere):
    scat = meta.ScatteringPattern(
        name='Hi', environment=env, creater=person, frequencies=[100, 200],
        incident_coords=half_sphere, is_reference=False,
        mics_coords=half_sphere)
    assert scat.name == 'Hi'
    assert scat.creater == person
    assert scat.environment == env
    assert scat.mics_coords == half_sphere
    assert scat.incident_coords == half_sphere
    npt.assert_allclose(scat.incident_azimuth_rad, scat.mics_azimuth_rad)
    npt.assert_allclose(scat.incident_colatitude_rad, scat.mics_colatitude_rad)
    npt.assert_allclose(scat.incident_radius, scat.mics_radius)
    npt.assert_allclose(
        scat.incident_azimuth_rad, half_sphere.get_sph()[:, 1, 0])
    npt.assert_allclose(
        scat.incident_colatitude_rad, half_sphere.get_sph()[1, :, 1])
    assert scat.incident_radius == 10.
    assert scat.is_reference is False
    npt.assert_array_almost_equal(scat.frequencies, np.array([100, 200]))


def test_read_write_sample(tmpdir, sample):
    filename = sample.write(tmpdir)
    sample2 = meta.Meta.open(filename)
    assert sample2.__class__ == sample.__class__
    assert sample2 == sample


def test_read_write_scattering_pattern(tmpdir, scattering_pattern):
    filename = scattering_pattern.write(tmpdir)
    meas2 = meta.Meta.open(filename)
    assert meas2.__class__ == scattering_pattern.__class__
    assert meas2 == scattering_pattern
    # assert meas2.name == scattering_pattern.name
    # assert meas2.creater == scattering_pattern.creater
    # assert meas2.environment == scattering_pattern.environment
    # npt.assert_allclose(meas2.frequencies, scattering_pattern.frequencies)
    # npt.assert_allclose(
    #     meas2.incident_coords.get_cart(),
    #     scattering_pattern.incident_coords.get_cart())
    # npt.assert_allclose(
    #     meas2.mics_coords.get_cart(),
    #     scattering_pattern.mics_coords.get_cart())
    # npt.assert_allclose(
    #     meas2.incident_azimuth_rad, scattering_pattern.incident_azimuth_rad)
    # npt.assert_allclose(
    #     meas2.incident_colatitude_rad,
    #     scattering_pattern.incident_colatitude_rad)
    # npt.assert_allclose(
    #     meas2.incident_radius, scattering_pattern.incident_radius)
    # npt.assert_allclose(meas2.mics_radius, scattering_pattern.mics_radius)
    # npt.assert_allclose(
    #     meas2.mics_azimuth_rad, scattering_pattern.mics_azimuth_rad)
    # npt.assert_allclose(
    #     meas2.mics_colatitude_rad, scattering_pattern.mics_colatitude_rad)
    # npt.assert_allclose(
    #     meas2.mics_coords.get_cart(),
    #     scattering_pattern.mics_coords.get_cart())
    # npt.assert_allclose(
    #     meas2.mics_coords.get_cart(),
    #     scattering_pattern.mics_coords.get_cart())


def test_read_write_scattering_random(tmpdir, scattering_random):
    filename = scattering_random.write(tmpdir)
    meas2 = meta.Meta.open(filename)
    assert meas2.__class__ == scattering_random.__class__
    assert meas2 == scattering_random


def test_read_write_scattering_coefficient(tmpdir, scattering_coefficient):
    filename = scattering_coefficient.write(tmpdir)
    meas2 = meta.Meta.open(filename)
    assert meas2.__class__ == scattering_coefficient.__class__
    assert meas2 == scattering_coefficient


def test_read_write_environment(tmpdir, env):
    filename = env.write(tmpdir)
    meas2 = meta.Meta.open(filename)
    assert meas2.__class__ == env.__class__
    assert meas2 == env


def test_read_write_person(tmpdir, person):
    filename = person.write(tmpdir)
    meas2 = meta.Meta.open(filename)
    assert meas2.__class__ == person.__class__
    assert meas2 == person


def test_read_write_comsol_bem(tmpdir, comsol_bem):
    filename = comsol_bem.write(tmpdir)
    meas2 = meta.Meta.open(filename)
    assert meas2.__class__ == comsol_bem.__class__
    assert meas2 == comsol_bem
