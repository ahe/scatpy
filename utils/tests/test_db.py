from utils import db
from sqlmodel import Session
import pytest
import pyfar as pf
import os


def test_scattering_coefficient_init():
    s1 = db.ScatteringCoefficient(
        name='sine', hight=1, wavelength=1, dimension=1,
        reference='book', description='sine wave', frequencies=[1, 2],
        coefficients=[0.1, 0.2])
    assert s1.name == 'sine'
    assert s1.hight == 1.
    assert s1.wavelength == 1.
    assert s1.reference == 'book'
    assert s1.description == 'sine wave'
    assert s1.frequencies == [1.0, 2.0]
    assert s1.coefficients == [.1, .2]


def test_scattering_coefficient_description_optional():
    s1 = db.ScatteringCoefficient(
        name='sine', hight=1, wavelength=1, dimension=1,
        reference='book', frequencies=[1, 2], coefficients=[0.1, 0.2])
    assert s1.name == 'sine'
    assert s1.hight == 1.
    assert s1.wavelength == 1.
    assert s1.reference == 'book'
    assert s1.frequencies == [1.0, 2.0]
    assert s1.coefficients == [.1, .2]


def test_scattering_coefficient_init_error():
    with pytest.raises(ValueError, match='same length'):
        db.ScatteringCoefficient(
            name='sine', hight=1, wavelength=1, dimension=1,
            reference='book', description='sine wave', frequencies=[1, 2],
            coefficients=[0.1, 0.2, 0.3])


def test_init_db(tmpdir):
    sqlite_file_name = "database.db"
    sqllite_path = os.path.join(tmpdir, sqlite_file_name)
    sqlite_url = f"sqlite:///{sqllite_path}"
    db.init(sqlite_url)
    assert True


def test_init_scattering(test_session):
    s1 = db.ScatteringCoefficient(
        name='sine', hight=1, wavelength=1, dimension=1,
        reference='book', description='sine wave', frequencies=[100, 200],
        coefficients=[0.1, 0.2])
    test_session.add(s1)
    test_session.commit()
    s = test_session.query(db.ScatteringCoefficient).all()
    assert s1 == s[0]
    assert 1 == len(s)


def test_add_scattering_coefficient(test_session, scattering_coefficient):
    db.add_scattering_coefficient(
        test_session, 'hasta la vista', pf.FrequencyData([0.1, 0.2], [100, 200]),
        scattering_coefficient, 1)
    s = test_session.query(db.ScatteringCoefficient).all()
    sample = scattering_coefficient.environment.sample
    assert s[0].name == 'hasta la vista'
    assert s[0].hight == sample.surf_height_m
    assert s[0].wavelength == sample.surf_wavelength_m
    assert s[0].reference == scattering_coefficient.environment.name
    assert s[0].description == scattering_coefficient.environment.name
    assert s[0].frequencies == [100.0, 200.0]
    assert s[0].coefficients == [.1, .2]
