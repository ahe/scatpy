from pyfar import Coordinates
import pytest
from utils import meta, spatial, db, folders
import numpy as np
import os


@pytest.fixture
def incident_angles_rad() -> dict:
    return {"theta": [0, 1], "phi": [0, 1]}


@pytest.fixture
def mics_angles_rad() -> dict:
    return {"theta": [0, 1], "phi": [0, 1]}


@pytest.fixture
def half_sphere() -> Coordinates:
    delta = 10
    r = 10
    phi_rad = np.linspace(0, 2*np.pi, num=int(360/delta)+1)
    theta_rad = np.linspace(0, np.pi/2, num=int(90/delta)+1)
    phi, theta = np.meshgrid(phi_rad, theta_rad, indexing='ij')
    return Coordinates(
        phi, theta, np.ones(phi.shape)*r, 'sph')


@pytest.fixture
def sample() -> meta.Sample:
    return meta.Sample(name='sine')


@pytest.fixture
def person() -> meta.Person:
    return meta.Person(name='fjo', given_name='Fritz', family_name='Josef')


@pytest.fixture
def env(sample) -> meta.Environment:
    return meta.Environment(name='hi', sample=sample)


@pytest.fixture
def scattering_pattern(
        person, env, half_sphere) -> meta.ScatteringPattern:
    return meta.ScatteringPattern(
        name='Hi', environment=env, creater=person, frequencies=[100, 200],
        mics_coords=half_sphere, is_reference=False,
        incident_coords=half_sphere)


@pytest.fixture
def scattering_coefficient(
        person, env, half_sphere) -> meta.ScatteringCoefficient:
    return meta.ScatteringCoefficient(
        name='Hi', environment=env, creater=person, frequencies=[100, 200],
        incident_coords=half_sphere)


@pytest.fixture
def scattering_random(person, env) -> meta.ScatteringCoefficientRandom:
    return meta.ScatteringCoefficientRandom(
        name='Hi', environment=env, creater=person, frequencies=[100, 200])


@pytest.fixture
def comsol_bem(sample) -> meta.ComsolBem:
    return meta.ComsolBem(name='hi', sample=sample)


@pytest.fixture
def coords_10_deg() -> Coordinates:
    all_az = np.linspace(0, 2*np.pi, 37)
    all_az[-1] = 0
    return spatial.angles2coords(
        all_az, np.linspace(0, np.pi/2, 10), 1)


@pytest.fixture
def coords_1_deg() -> Coordinates:
    all_az = np.linspace(0, 2*np.pi, 361)
    all_az[-1] = 0
    return spatial.angles2coords(
        all_az, np.linspace(0, np.pi/2, 91), 1)


@pytest.fixture
def test_session(tmpdir):
    sqlite_file_name = "database.sqlite"
    sqllite_path = os.path.join(tmpdir, sqlite_file_name)
    if os.path.exists(sqllite_path):
        os.remove(sqllite_path)
    sqlite_url = f"sqlite:///{sqllite_path}"
    session = db.init(sqlite_url)
    return session
