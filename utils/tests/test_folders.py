import utils.folders as folders
import os

def test_folders_test_path():
    if os.name == 'nt':  # Windows detected
        path = folders.test_path().split('\\')
    else:
        path = folders.test_path().split('/')
    assert path[-1] == 'tests'
    assert path[-2] == 'utils'


def test_folders_data_path():
    if os.name == 'nt':  # Windows detected
        path = folders.data_path().split('\\')
    else:
        path = folders.data_path().split('/')
    assert path[-1] == 'data'
