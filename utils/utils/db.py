from sqlalchemy import create_engine, ForeignKey, Column, String, Integer, Float, JSON
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.dialects.postgresql import ARRAY
from sqlalchemy.orm import sessionmaker, Session
from sqlalchemy.orm import validates
import os
from utils import folders, meta
import pyfar as pf

Base = declarative_base()


class ScatteringCoefficient(Base):
    __tablename__ = "scattering_coeffients"
    id = Column('id', Integer, primary_key=True, autoincrement=True)
    name = Column('name', String)
    hight = Column('hight', Float)
    wavelength = Column('wavelength', Float)
    dimension = Column('dimension', Integer)
    reference = Column('reference', String)
    frequencies = Column('frequencies', JSON())
    coefficients = Column('coefficients', JSON())
    description = Column('description', String, default='', nullable=True)

    def __init__(
            self, name, hight, wavelength, dimension, reference,
            frequencies, coefficients, description='') -> None:
        if len(frequencies) != len(coefficients):
            raise ValueError("data needs to have the same length.")
        self.name = name
        self.hight = hight
        self.wavelength = wavelength
        self.dimension = dimension
        self.reference = reference
        self.frequencies = frequencies
        self.coefficients = coefficients
        self.description = description
        super().__init__()


def init(sqlite_url=None):
    """Initilize or open the database and return the session.

    Parameters
    ----------
    sqlite_url : url, optional
        describes the url of the database, e.g. 'sqlite:///database.sqlite',
        by default the database in the data directory.

    Returns
    -------
    Session
        session handle of the database connection.
    """
    # Using SQLite here but can easily use PostgreSQL by changing the url
    if sqlite_url is None:
        sqlite_file_name = "database.sqlite"
        sqllite_path = os.path.join(folders.data_path(), sqlite_file_name)
        sqlite_url = f"sqlite:///{sqllite_path}"

    # The engine is the interface to our database so we can execute SQL
    engine = create_engine(sqlite_url)

    # using the engine we create the tables we need if they aren't already done
    Base.metadata.create_all(engine)
    Session = sessionmaker(engine)
    return Session()


def add_scattering_coefficient(
        session: Session, name: str, data: pf.FrequencyData,
        meta: meta.ScatteringCoefficient, dimension=1):
    """add a scattering coefficient to the database based on a
    pyfar.FrequencyData object with metadata.

    Parameters
    ----------
    session : Session
        session handle of the database connection.
    name : string
        name of the database entry
    data : pf.FrequencyData
        input scattering coefficient object with cdim=1.
    meta : meta.ScatteringCoefficient
        meta data object for additional information such as surface and
        environmental information.
    dimension : int, optional
        surface dimention, can be 1 or 2, by default 1
    """
    sample = meta.environment.sample
    f = [float(f) for f in data.frequencies]
    s = [float(f) for f in data.freq.flatten()]
    s1 = ScatteringCoefficient(
        name=name, hight=sample.surf_height_m,
        wavelength=sample.surf_wavelength_m, dimension=dimension,
        reference=meta.environment.name, description=meta.environment.name,
        frequencies=f, coefficients=s)
    session.add(s1)
    session.commit()
