import utils.meta as meta
import utils.folders as folders
import pyfar as pf
import os.path


def read_comsol_set_with_meta(path_far):
    data = pf.io.read(path_far)
    path_meta = f'{path_far[:-4]}.json'
    meta_pattern = meta.Meta.open(path_meta)
    return data['scattering'], meta_pattern


def write_comsol_set_with_meta(
        data: pf.FrequencyData, meta_pattern: meta.Meta,
        path: str = None) -> str:
    if path is None:
        path = os.path.join(folders.sciebo_data_path(), 'scatpy')
    meta_filename = meta_pattern.write(path)
    far_path = os.path.join(path, f'{meta_filename[:-5]}.far')
    pf.io.write(far_path, scattering=data)
    return far_path
