import pyfar as pf
import matplotlib.pyplot as plt
import numpy as np
"""Plot utilities and functions
"""


def cm2inch(*tupl):
    """Convert from cm to inches.
    Matplotlib uses inches as default unit.

    Conversion supports tuples as the figsize option for figure.

    """
    inch = 2.54
    if isinstance(tupl[0], tuple):
        return tuple(i/inch for i in tupl[0])
    else:
        return tuple(i/inch for i in tupl)


def scattering_polar(
        data: pf.FrequencyData, data_ref: pf.FrequencyData,
        coords_inc: pf.Coordinates, coords_mic: pf.Coordinates,
        phi: float, theta: float, frequency: float):
    index_phi, _ = coords_inc[:, 0].find_slice(
        'azimuth', 'deg', phi, 1)
    index_theta, _ = coords_inc[0, :].find_slice(
        'colatitude', 'deg', theta, 1)
    index_freq = data.frequencies.tolist().index(frequency)
    phi_mesh = coords_mic.get_sph()[..., 0]
    theta_mesh = coords_mic.get_sph()[..., 1]
    data_polar = data[
        index_phi[0][0], index_theta[0][0], :, :].freq[..., index_freq].copy()
    data_polar_ref = data_ref[
        index_phi[0][0], index_theta[0][0], :, :].freq[..., index_freq].copy()

    fig, ax = plt.subplots(1, 2, dpi=150, subplot_kw=dict(projection='polar'))
    ax[0].contourf(
        phi_mesh, theta_mesh*180/np.pi, np.abs(data_polar),
        100, cmap='jet')
    ax[1].contourf(
        phi_mesh, theta_mesh*180/np.pi, np.abs(data_polar_ref),
        100, cmap='jet')
    ax[0].plot(phi*np.pi/180, theta, 'ro')
    ax[0].set_title(f'sample\nf = {frequency} Hz')
    ax[1].plot(phi*np.pi/180, theta, 'ro')
    ax[1].set_title(f'reference\nf = {frequency} Hz', )
    plt.show()
