import dataclasses
import datetime
import json
import warnings
import os
import re
from attr import field
import numpy as np
from pyfar import Coordinates


@dataclasses.dataclass(kw_only=True, frozen=True)
class Meta:
    id: str = None
    name: str
    date: datetime.date = dataclasses.field(default=datetime.date.today())

    def write(self, path: str) -> str:
        path_meta = os.path.join(path, self.__class__.__name__)
        if not os.path.exists(path_meta):
            os.mkdir(path_meta)
        filename = f"{self.id}.{self.__class__.__name__}.json"
        meta_dict = self.__dict__.copy()
        for key in meta_dict:
            if isinstance(meta_dict[key], Meta):
                meta_dict[key].write(path)
                classname = meta_dict[key].__class__.__name__
                meta_dict[key] = f'meta.{classname}\\{meta_dict[key].id}.{classname}'
            if isinstance(meta_dict[key], datetime.date):
                meta_dict[key] = meta_dict[key].strftime("%Y-%m-%d")
            if isinstance(meta_dict[key], Coordinates):
                meta_dict[key] = meta_dict[key].get_cart().tolist()
            if isinstance(meta_dict[key], np.ndarray):
                meta_dict[key] = meta_dict[key].tolist()
        full_path = os.path.join(path_meta, filename)
        if os.path.exists(full_path):
            data = Meta.open(full_path)
            if self != data:
                warnings.warn(
                    f'{data.name} != {self.name} of type {type(data)}')

        with open(full_path, 'w', encoding='utf-8') as outfile:
            json.dump(meta_dict, outfile, indent=4, ensure_ascii=False)
        return full_path

    def open(path: str):
        root = os.path.dirname(path)
        root = os.path.join(root, '..')
        with open(path, 'r') as f:
            data = json.load(f)
        for key in data:
            date_search = '[0-9]{4}-[0-9]{2}-[0-9]{2}'
            if not isinstance(data[key], (str, list)):
                continue
            if isinstance(data[key], str):
                if len(data[key]) == 10 and re.search(
                        date_search, data[key]) is not None:
                    data[key] = datetime.datetime.strptime(
                        data[key], '%Y-%m-%d').date()
                elif len(data[key]) > 5 and data[key][:4] == 'meta':
                    sub_path = os.path.join(root, f'{data[key][5:]}.json')
                    sub_path = os.path.join(
                        sub_path.split('\\')[0], sub_path.split('\\')[1])
                    data[key] = Meta.open(sub_path)
            elif np.array(data[key]).shape[-1] == 3:
                xyz = np.array(data[key])
                x = xyz[..., 0].tolist()
                y = xyz[..., 1].tolist()
                z = xyz[..., 2].tolist()
                data[key] = f'Coordinates({x}, {y}, {z})'
            elif key.endswith('_rad'):
                data[key] = []
        class_name = path.split('.')[-2]
        statement = f'{class_name}(**{data})'
        statement = re.sub('inf', 'np.inf', statement)
        statement = re.sub('nan', 'np.nan', statement)
        return eval(statement)

    def build_identifier(self):
        id = f'{self.name}'
        if self.id is not None and self.id != id:
            warnings.warn('Given id is wrong an will be overwritten.')
        object.__setattr__(self, 'id', id)

    @property
    def date_str(self) -> str:
        return self.date.strftime('%Y-%m-%d')


@dataclasses.dataclass(kw_only=True, frozen=True)
class Sample(Meta):
    diameter_m: float = 0.8
    surf_wavelength_m: float = 0.177/2.5
    surf_height_m: float = 0.051/2.5
    surf_reflection: float = 1.
    body_hight_m: float = 0.03
    body_reflection: float = 1
    scale_factor: float = 1

    def __post_init__(self):
        if self.body_reflection > 1 or self.body_reflection < -1:
            warnings.warn(
                'Reflection factor must be between -1 and 1. '
                f'It is {self.body_reflection}')
            body_reflection = max(min(self.body_reflection, 1), -1)
            object.__setattr__(self, 'body_reflection', body_reflection)

        if self.surf_reflection > 1 or self.surf_reflection < -1:
            warnings.warn(
                'Reflection factor must be between -1 and 1. '
                f'It is {self.surf_reflection}')
            surf_reflection = max(min(self.surf_reflection, 1), -1)
            object.__setattr__(self, 'surf_reflection', surf_reflection)

        self.build_identifier()


@dataclasses.dataclass(kw_only=True, frozen=True)
class Environment(Meta):
    name: str
    sample: Sample

    def __post_init__(self):
        self.build_identifier()


@dataclasses.dataclass(kw_only=True, frozen=True)
class ComsolBem(Environment):
    comsol_version: str = ''
    model_name: str = ''

    def __post_init__(self):
        self.build_identifier()


@dataclasses.dataclass(kw_only=True, frozen=True)
class Mesh2Hrtf(Environment):
    version: str = ''
    model_name: str = ''

    def __post_init__(self):
        self.build_identifier()


@dataclasses.dataclass(kw_only=True, frozen=True)
class Person(Meta):
    given_name: str
    family_name: str
    affiliation: str = 'Institute for Hearing Technology and Acoustics, '\
        'RWTH Aachen University'

    def __post_init__(self):
        self.build_identifier()


@dataclasses.dataclass(kw_only=True, frozen=True, eq=False)
class ScatteringCoefficientRandom(Meta):
    name: str
    creater: Person
    environment: Environment
    frequencies: np.ndarray

    def __post_init__(self):
        self.build_identifier()
        object.__setattr__(
            self, 'frequencies', np.array(self.frequencies))

    def _coordinates2dict(self, coords: Coordinates) -> dict:
        azimuth = coords.get_sph()[:, 1, 0]
        colatitude = coords.get_sph()[1, :, 1]
        radius = np.round(coords.get_sph()[0, 0, 2], 15)
        return azimuth, colatitude, radius

    def __eq__(self, other):
        return dc_eq(self, other)


@dataclasses.dataclass(kw_only=True, frozen=True, eq=False)
class ScatteringPattern(ScatteringCoefficientRandom):
    is_reference: bool
    incident_azimuth_rad: np.ndarray = field(init=False)
    incident_colatitude_rad: np.ndarray = field(init=False)
    incident_radius: float = field(init=False)
    mics_azimuth_rad: np.ndarray = field(init=False)
    mics_colatitude_rad: np.ndarray = field(init=False)
    mics_radius: float = field(init=False)
    incident_coords: Coordinates = field(eq=False)
    mics_coords: Coordinates = field(eq=False)

    def __post_init__(self):
        object.__setattr__(
            self, 'frequencies', np.array(self.frequencies))

        self.build_identifier()

        # convert Coordinate objects to objects if they are str
        if isinstance(self.mics_coords, str):
            mics_coords = eval(self.mics_coords)
            mics_coords.get_sph(convert=True)
            object.__setattr__(self, 'mics_coords', mics_coords)
        if isinstance(self.incident_coords, str):
            incident_coords = eval(self.incident_coords)
            incident_coords.get_sph(convert=True)
            object.__setattr__(self, 'incident_coords', incident_coords)

        #
        azimuth, colatitude, radius = self._coordinates2dict(self.mics_coords)
        object.__setattr__(self, 'mics_azimuth_rad', azimuth)
        object.__setattr__(self, 'mics_colatitude_rad', colatitude)
        object.__setattr__(self, 'mics_radius', radius)
        azimuth, colatitude, radius = self._coordinates2dict(
            self.incident_coords)
        object.__setattr__(self, 'incident_azimuth_rad', azimuth)
        object.__setattr__(self, 'incident_colatitude_rad', colatitude)
        object.__setattr__(self, 'incident_radius', radius)

    def __eq__(self, other):
        return dc_eq(self, other)


@dataclasses.dataclass(kw_only=True, frozen=True, eq=False)
class ScatteringCoefficient(ScatteringCoefficientRandom):
    incident_azimuth_rad: np.ndarray = field(init=False)
    incident_colatitude_rad: np.ndarray = field(init=False)
    incident_radius: float = field(init=False)
    incident_coords: Coordinates = field(eq=False)

    def __eq__(self, other):
        return dc_eq(self, other)

    def __post_init__(self):
        object.__setattr__(
            self, 'frequencies', np.array(self.frequencies))

        self.build_identifier()

        if isinstance(self.incident_coords, str):
            incident_coords = eval(self.incident_coords)
            incident_coords.get_sph(convert=True)
            object.__setattr__(self, 'incident_coords', incident_coords)

        azimuth, colatitude, radius = self._coordinates2dict(
            self.incident_coords)
        object.__setattr__(self, 'incident_azimuth_rad', azimuth)
        object.__setattr__(self, 'incident_colatitude_rad', colatitude)
        object.__setattr__(self, 'incident_radius', radius)


def dc_eq(dc1, dc2) -> bool:
    """checks if two dataclasses which hold numpy arrays are equal"""
    if dc1 is dc2:
        return True
    if dc1.__class__ is not dc2.__class__:
        return NotImplemented  # better than False
    t1 = dataclasses.astuple(dc1)
    t2 = dataclasses.astuple(dc2)

    return all(array_safe_eq(a1, a2) for a1, a2 in zip(t1, t2))


def array_safe_eq(a, b) -> bool:
    """Check if a and b are equal, even if they are numpy arrays"""
    if isinstance(a, Coordinates) and isinstance(b, Coordinates):
        return True
    if a is b:
        return True
    if isinstance(a, np.ndarray) and isinstance(b, np.ndarray):
        return a.shape == b.shape and (np.abs(a - b) < 5e-15).all()
    try:
        return a == b
    except TypeError:
        return NotImplemented
