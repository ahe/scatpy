# Export the 'Scattering from a rigid sphere' tutorial

import numpy as np
import os
import bpy

# Settings
project_name_out = 'ita_200k'
project_name_in = 'ita_200k'
program_path = '/home/anne/git/mesh2hrtf/Mesh2HRTF/mesh2hrtf'
data_path = '/home/anne/sciebo/2021_DFG-Projekt/data'
file_path = os.path.join(data_path, 'mesh2hrtf_results', project_name_out)

# run two times
# blender --background --python process/mesh2hrtf/mesh2scattering.py


def create_scene_with_stl(
        stl_path, stl_name, source_distance, source_theta_deg, source_phi_deg):
    id = stl_name.replace(' ', '_').lower()
    name = f'{id}'
    folder_out = f'{file_path}/{name}'
    if os.path.exists(folder_out):
        return False
    # prepare the scene -------------------------------------------------------
    # force object mode
    bpy.ops.object.mode_set(mode='OBJECT', toggle=False)

    # set cursor to origin
    bpy.context.scene.cursor.location = (0.0, 0, 0.0)

    # remove cube
    bpy.data.objects['Cube'].select_set(True)
    bpy.ops.object.delete()

    # add mesh
    bpy.ops.import_mesh.stl(
        filepath=stl_path, axis_forward='Y', axis_up='Z')
    bpy.data.objects[stl_name].name = 'Reference'

    # add point source
    id = 0
    for theta in source_theta_deg:
        for phi in source_phi_deg:
            theta_rad = theta * np.pi / 180.
            phi_rad = phi * np.pi / 180.
            x = source_distance * np.sin(theta_rad) * np.cos(phi_rad)
            y = source_distance * np.sin(theta_rad) * np.sin(phi_rad)
            z = source_distance * np.cos(theta_rad)

            bpy.ops.object.light_add(location=(x, y, z))
            point_name = 'Point source' if id == 0 else f'Point source{id}'
            bpy.data.objects['Point'].name = point_name

    # export the project ------------------------------------------------------
    bpy.ops.mesh2input.inp(
        filepath=folder_out,
        programPath=program_path,
        title=name,
        method='ML-FMM BEM',
        sourceType='Point source',
        pictures=False,
        unit='m',
        speedOfSound='343.18',
        evaluationGrids='coords5_',
        minFrequency=1000,
        maxFrequency=16000,
        frequencyVectorValue=1000
    )
    bpy.ops.wm.quit_blender()
    return True


meshes_path = os.path.join(data_path, 'meshes')
sample_path = os.path.join(meshes_path, project_name_in, 'sample.stl')
ref_path = os.path.join(meshes_path, project_name_in, 'reference.stl')
source_distance = 10
source_phi_deg = [0, 10, 20, 30, 40, 50, 60, 70, 80, 90]
source_theta_deg = [10, 20, 30, 40, 50, 60, 70, 80]
if not os.path.exists(file_path):
    os.mkdir(file_path)

for itype in [0, 1]:
    if itype == 0:
        created = create_scene_with_stl(
            sample_path, 'Sample',
            source_distance, source_theta_deg, source_phi_deg)
        if created:
            break

    if itype == 1:
        created = create_scene_with_stl(
            ref_path, 'Reference', source_distance, source_theta_deg, [0])
        if created:
            break
