# %%
import mesh2hrtf as m2h
import os

# %%
path = '/home/anne/git/mesh2hrtf/Mesh2HRTF/mesh2hrtf/Mesh2Input/EvaluationGrids/Data'
coords = m2h.read_evaluation_grid(os.path.join(path, 'coords5'))
m2h.write_evaluation_grid(coords, os.path.join(path, 'coords5_'), 700000)

# %%
