# %%
from utils import io,  meta, spatial, m2s, folders
import numpy as np
import pyfar as pf
import imkar as ik
import os

# Set folder names
project_folders = ['ita_oct', 'ita_100k', 'ita_3rd']
project_folders = [
    'sine', '01_kunsthaus_zuerich', 'ita_oct', 'ita_100k', 'ita_3rd']
# project_folders = ['sine']
scale_factors = [2.5, 20, 100, 100, 100]
# scale_factors = [2.5]

# %%
assert len(scale_factors) == len(project_folders)
for idx in range(len(project_folders)):
    project_folder = project_folders[idx]
    scale_factor = scale_factors[idx]
    # read all data
    sample = 'sample'
    sample_ref = 'reference'
    sciebo_data_path = folders.sciebo_data_path()
    simulation_name = f'mesh2hrtf_{project_folder}'
    path = os.path.join(
        sciebo_data_path, 'mesh2hrtf_results', project_folder, sample)
    data, mic_coords, inc_coords, params = m2s.read_numcalc(path)

    simulation_name_ref = f'mesh2hrtf_{project_folder}_ref'
    path = os.path.join(
        sciebo_data_path, 'mesh2hrtf_results', project_folder, sample_ref)
    data_ref, mic_coords_ref, inc_coords_ref, params_ref = m2s.read_numcalc(
        path)

    radius = 1
    all_az = np.linspace(0, 2*np.pi, 361)
    all_az[-1] = 0
    all_el = np.linspace(0, np.pi/2, 91)
    mic_coords_out = pf.Coordinates(
        *np.meshgrid(all_az, all_el, indexing='ij'),
        radius, 'sph', 'top_colat')

    radius = 10
    all_az = np.linspace(0, np.pi/2, 10)
    all_el = np.linspace(np.pi/18, np.pi/2-np.pi/18, 8)
    inc_coords_out = pf.Coordinates(
        *np.meshgrid(all_az, all_el, indexing='ij'),
        radius, 'sph', 'top_colat')
    inc_phi = np.linspace(0, 360, 37)

    # post process sample pressure

    # reshape data for scattering
    data = spatial.reshape_to_az_by_el(data, inc_coords, inc_coords_out)
    data = spatial.reshape_to_az_by_el(data, mic_coords, mic_coords_out, 2)
    mic_coords = mic_coords_out.copy()
    inc_coords = inc_coords_out.copy()

    # apply symmetry
    data.freq = np.moveaxis(data.freq, 0, -2)
    data.freq = np.moveaxis(data.freq, 0, -2)
    data, inc_coords = spatial.apply_symmetry_mirror(
        data, mic_coords, inc_coords, 2)
    data, inc_coords = spatial.apply_symmetry_mirror(
        data, mic_coords, inc_coords, 2)
    data.freq = np.moveaxis(data.freq, 3, 0)
    data.freq = np.moveaxis(data.freq, 3, 0)

    # Add Meta data
    ahe = meta.Person(name='ahe', given_name='Anne', family_name='Heimes')
    sine_sample = meta.Sample(
        name=project_folder, diameter_m=0.8, scale_factor=scale_factor,
        surf_wavelength_m=0.0468, surf_height_m=0.0204,
        surf_reflection=1, body_hight_m=0.03, body_reflection=1)
    mesh2hrtf = meta.Mesh2Hrtf(
        name=simulation_name, sample=sine_sample,
        version=params['Mesh2HRTF_Version'],
        model_name=project_folder)
    meta_pattern = meta.ScatteringPattern(
        name=simulation_name, creater=ahe, environment=mesh2hrtf,
        frequencies=data.frequencies, is_reference=False,
        incident_coords=inc_coords, mics_coords=mic_coords)

    # write data
    file_path_sample = io.write_comsol_set_with_meta(data, meta_pattern)
    print(file_path_sample)

    # post process reference pressure

    # reshape data for scattering
    data_ref = spatial.reshape_to_az_by_el(
        data_ref, mic_coords_ref, mic_coords_out, 1)
    mic_coords_ref = mic_coords_out.copy()

    # apply symmetry
    data_ref.freq = np.moveaxis(data_ref.freq, 0, -2)
    data_ref, inc_coords_ref = spatial.apply_symmetry_circular(
        data_ref, mic_coords_ref, inc_coords_ref, inc_phi, 2)

    # Add Meta data
    ahe = meta.Person(name='ahe', given_name='Anne', family_name='Heimes')
    ref_sample = meta.Sample(
        name=f'{project_folder}_ref', diameter_m=0.8,
        scale_factor=scale_factor,
        surf_wavelength_m=0, surf_height_m=0, surf_reflection=1,
        body_hight_m=0.0402, body_reflection=1)
    mesh2hrtf = meta.Mesh2Hrtf(
        name=simulation_name_ref, sample=ref_sample,
        version=params_ref['Mesh2HRTF_Version'],
        model_name=project_folder)
    meta_pattern_ref = meta.ScatteringPattern(
        name=simulation_name_ref, creater=ahe, environment=mesh2hrtf,
        frequencies=data_ref.frequencies, is_reference=False,
        incident_coords=inc_coords_ref, mics_coords=mic_coords_ref)

    # write data
    file_path_sample = io.write_comsol_set_with_meta(
        data_ref, meta_pattern_ref)
    print(file_path_sample)

    # calculate scattering coefficient
    s = ik.scattering.coefficient.freefield(
        data, data_ref, meta_pattern.mics_coords)
    scattering_meta = meta.ScatteringCoefficient(
        name=f'{meta_pattern.name}',
        creater=meta_pattern.creater,
        environment=meta_pattern.environment,
        frequencies=s.frequencies,
        incident_coords=meta_pattern.incident_coords)
    file_path_s = io.write_comsol_set_with_meta(s, scattering_meta)
    print(file_path_s)

    s_rand = ik.scattering.coefficient.random_incidence(
        s, meta_pattern.incident_coords)
    scattering_rand_meta = meta.ScatteringCoefficientRandom(
        name=f'{meta_pattern.name}',
        creater=meta_pattern.creater,
        environment=meta_pattern.environment,
        frequencies=s.frequencies)
    file_path_s_rand = io.write_comsol_set_with_meta(
        s_rand, scattering_rand_meta)
    print(file_path_s_rand)

# %%
