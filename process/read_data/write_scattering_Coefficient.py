# %%
from utils import io,  meta, spatial, m2s, folders
import numpy as np
import pyfar as pf
import imkar as ik
import os

# %%
file = 'mesh2hrtf_ita'

# %%
# read data
folder = os.path.join(
    folders.sciebo_data_path(), 'scatpy', 'ScatteringPattern')
path = os.path.join(folder, f'{file}.ScatteringPattern.far')
data, meta_pattern = io.read_comsol_set_with_meta(path)
path = os.path.join(folder, f'{file}.ScatteringPattern.far')
data_ref, meta_pattern_ref = io.read_comsol_set_with_meta(path)

# %%
# calculate scattering coefficient
s = ik.scattering.coefficient.freefield(
    data, data_ref, meta_pattern.mics_coords)
scattering_meta = meta.ScatteringCoefficient(
    name=f'{meta_pattern.name}',
    creater=meta_pattern.creater,
    environment=meta_pattern.environment,
    frequencies=s.frequencies,
    incident_coords=meta_pattern.incident_coords)
file_path_s = io.write_comsol_set_with_meta(s, scattering_meta)
print(file_path_s)

s_rand = ik.scattering.coefficient.random_incidence(
    s, meta_pattern.incident_coords)
scattering_rand_meta = meta.ScatteringCoefficientRandom(
    name=f'{meta_pattern.name}',
    creater=meta_pattern.creater,
    environment=meta_pattern.environment,
    frequencies=s.frequencies)
file_path_s_rand = io.write_comsol_set_with_meta(
    s_rand, scattering_rand_meta)
print(file_path_s_rand)

# %%
