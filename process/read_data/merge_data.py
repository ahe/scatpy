# %%
import os
from utils import folders, io, m2s, meta
import imkar as ik

files = [
    'mesh2hrtf_ita_oct',
    'mesh2hrtf_ita_100k',
    'mesh2hrtf_ita_3rd',
    ]
out_name = 'mesh2hrtf_ita'

# %%
folder = os.path.join(
    folders.sciebo_data_path(), 'scatpy', 'ScatteringPattern')

data_list = []
data_ref_list = []
for idx, file in enumerate(files):
    path = os.path.join(folder, f'{file}.ScatteringPattern.far')
    data, meta_s_rand = io.read_comsol_set_with_meta(path)
    data_list.append(data)
    path = os.path.join(folder, f'{file}_ref.ScatteringPattern.far')
    data_ref, meta_s_rand_ref = io.read_comsol_set_with_meta(path)
    data_ref_list.append(data_ref)

data = m2s.merge_frequency_data(data_list)
data_ref = m2s.merge_frequency_data(data_ref_list)
del data_list, data_ref_list

meta_env = meta.Mesh2Hrtf(name=out_name, sample=meta_s_rand.environment.sample)
meta_pattern = meta.ScatteringPattern(
    name=out_name,
    creater=meta_s_rand.creater,
    environment=meta_env,
    frequencies=data.frequencies,
    is_reference=False,
    incident_coords=meta_s_rand.incident_coords,
    mics_coords=meta_s_rand.mics_coords)
meta_env_ref = meta.Mesh2Hrtf(
    name=f'{out_name}_ref', sample=meta_s_rand.environment.sample)
meta_pattern_ref = meta.ScatteringPattern(
    name=f'{out_name}_ref',
    creater=meta_s_rand_ref.creater,
    environment=meta_env_ref,
    frequencies=data_ref.frequencies,
    is_reference=True,
    incident_coords=meta_s_rand_ref.incident_coords,
    mics_coords=meta_s_rand_ref.mics_coords)

file_path_sample = io.write_comsol_set_with_meta(data, meta_pattern)
print(file_path_sample)

file_path_ref = io.write_comsol_set_with_meta(data_ref, meta_pattern_ref)
print(file_path_ref)

# %%
# calculate scattering coefficient
s = ik.scattering.coefficient.freefield(
    data, data_ref, meta_pattern.mics_coords)
scattering_meta = meta.ScatteringCoefficient(
    name=f'{meta_pattern.name}',
    creater=meta_pattern.creater,
    environment=meta_pattern.environment,
    frequencies=s.frequencies,
    incident_coords=meta_pattern.incident_coords)
file_path_s = io.write_comsol_set_with_meta(s, scattering_meta)
print(file_path_s)

s_rand = ik.scattering.coefficient.random_incidence(
    s, meta_pattern.incident_coords)
scattering_rand_meta = meta.ScatteringCoefficientRandom(
    name=f'{meta_pattern.name}',
    creater=meta_pattern.creater,
    environment=meta_pattern.environment,
    frequencies=s.frequencies)
file_path_s_rand = io.write_comsol_set_with_meta(
    s_rand, scattering_rand_meta)
print(file_path_s_rand)

# %%
