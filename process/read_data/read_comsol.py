# %%
import numpy as np
import utils.meta as meta
import utils.io as io
import utils.folders as folders
import utils.spatial as spatial
import os
import pyfar as pf
import re
import imkar as ik

models = ['point_120', 'point_230', 'point_1000', 'plane']
r_mics = [1, 100]
# models = ['point_1000']
# r_mics = [100]
scale_factors = [2.5, 2.5, 2.5, 2.5]

# %% write
sciebo_data_path = folders.sciebo_data_path()
files_dir = os.path.join(sciebo_data_path, 'simulation_results')
path_out = os.path.join(sciebo_data_path, 'scatpy', 'ScatteringPattern')
if not os.path.exists(path_out):
    os.mkdir(path_out)
# Path(path_out).mkdir(parents=True, exist_ok=True)
ahe = meta.Person(name='ahe', given_name='Anne', family_name='Heimes')

for idx in range(len(models)):
    for r_mic in r_mics:
        model_name = models[idx]
        scale_factor = scale_factors[idx]

        sine_sample = meta.Sample(
            name='sine', diameter_m=0.8, scale_factor=scale_factor,
            surf_wavelength_m=0.0468, surf_height_m=0.0204,
            surf_reflection=1, body_hight_m=0.03, body_reflection=1)
        file_name_sample = f'{model_name}-done_r_mic{r_mic}.csv'
        file_name_reference = f'{model_name}_reference-done_r_mic{r_mic}.csv'

        radius_inc = 10000. if model_name in 'plane' else 1.2
        frequencies = [1250., 1600., 2000., 2500., 3150., 4000., 5000.]
        name_sim = f'{model_name}_{r_mic}'

        # read sample data
        comsol60 = meta.ComsolBem(
            name=name_sim, sample=sine_sample,
            comsol_version='6.0.0.354', model_name=name_sim)
        for index, freq in enumerate(frequencies):
            name = re.sub('done', f'{round(freq)}', file_name_sample)
            data_out, mic_coords = pf.io.read_comsol(
                os.path.join(files_dir, name))
            if index == 0:
                data = data_out.freq
                _, _, parameters, _, _ = pf.io.read_comsol_header(
                    os.path.join(files_dir, name))
            else:
                data = np.concatenate((data, data_out.freq), axis=-1)
        data = pf.FrequencyData(np.squeeze(data), frequencies)
        all_az = np.linspace(0, 2*np.pi, 37)
        all_az[-1] = 0
        coords_out = spatial.angles2coords(
            all_az, np.linspace(0, np.pi/2, 10), 100)
        data = spatial.reshape_to_az_by_el(
            data, mic_coords, coords_out)
        mic_coords = coords_out.copy()
        inc_coords = spatial.angles2coords(
            parameters['phi'], parameters['theta'], radius_inc, unit='rad')
        data, inc_coords = spatial.apply_symmetry_mirror(
            data, mic_coords, inc_coords, 2)
        data, inc_coords = spatial.apply_symmetry_mirror(
            data, mic_coords, inc_coords, 2)
        meta_pattern = meta.ScatteringPattern(
            name=name_sim, creater=ahe, environment=comsol60,
            frequencies=data.frequencies, is_reference=False,
            incident_coords=inc_coords, mics_coords=mic_coords)
        data.freq = np.moveaxis(data.freq, -2, 0)
        data.freq = np.moveaxis(data.freq, -2, 0)
        file_path_sample = io.write_comsol_set_with_meta(data, meta_pattern)
        print(file_path_sample)

        # read reference data
        data_ref, mic_coords = pf.io.read_comsol(
            os.path.join(files_dir, file_name_reference))
        _, _, parameters, _, _ = pf.io.read_comsol_header(
            os.path.join(files_dir, file_name_reference))
        data_ref = spatial.reshape_to_az_by_el(
            data_ref, mic_coords, coords_out)
        mic_coords = coords_out.copy()
        inc_phi = inc_coords.get_sph(unit='deg')[:, 0, 0]
        inc_coords_ref = spatial.angles2coords(
            0, parameters['theta'], radius_inc, unit='rad')
        data_ref.freq = np.squeeze(data_ref.freq)
        data_ref, inc_coords_ref = spatial.apply_symmetry_circular(
            data_ref, mic_coords, inc_coords_ref, inc_phi, 2)

        file_path_ref = []
        for index, height in enumerate(parameters['h_sample']):
            name = f"{model_name}_{r_mic}_{height*100}cm_ref"
            ref_sample = meta.Sample(
                name=f'sine_ref_{height*100}cm', diameter_m=0.8,
                scale_factor=scale_factor,
                surf_wavelength_m=0, surf_height_m=0, surf_reflection=1,
                body_hight_m=height, body_reflection=1)
            comsol60 = meta.ComsolBem(
                name=name, sample=ref_sample,
                comsol_version='6.0.0.354', model_name=name)
            meta_pattern = meta.ScatteringPattern(
                name=name, creater=ahe, environment=comsol60,
                frequencies=data_ref.frequencies, is_reference=True,
                incident_coords=inc_coords_ref, mics_coords=mic_coords)
            file_path_ref.append(
                io.write_comsol_set_with_meta(
                    data_ref[:, :, :, :, index], meta_pattern))
            print(file_path_ref[-1])


# %% write scattering coefficients
root = path_out
date = '2022-12-19'

for model_name in models:
    for r_mic in r_mics:
        for h_ref in ['3.0', '4.02', '5.04']:
            pattern = 'ScatteringPattern'
            file_path_sample = f'{model_name}_{r_mic}.{pattern}.far'
            file_path_ref = f'{model_name}_{r_mic}_{h_ref}cm_ref.{pattern}.far'

            # Calculate scattering coefficient
            data, meta_pattern = io.read_comsol_set_with_meta(
                os.path.join(root, file_path_sample))
            data_ref, meta_pattern_ref = io.read_comsol_set_with_meta(
                os.path.join(root, file_path_ref))
            s = ik.scattering.coefficient.freefield(
                data, data_ref, meta_pattern.mics_coords)
            reference_hight = meta_pattern_ref.environment.sample.body_hight_m
            scattering_meta = meta.ScatteringCoefficient(
                name=f'{meta_pattern.name}_{reference_hight}',
                creater=meta_pattern.creater,
                environment=meta_pattern.environment,
                frequencies=s.frequencies,
                incident_coords=meta_pattern.incident_coords)
            print(io.write_comsol_set_with_meta(s, scattering_meta))

            # Calc Random incident scatterng coefficient
            s_rand = ik.scattering.coefficient.random_incidence(
                s, meta_pattern.incident_coords)
            scattering_rand_meta = meta.ScatteringCoefficientRandom(
                name=meta_pattern.name,
                creater=meta_pattern.creater,
                environment=meta_pattern.environment,
                frequencies=s.frequencies)
            file_path_s_rand = io.write_comsol_set_with_meta(
                s_rand, scattering_rand_meta)
            print(file_path_s_rand)

# %%
