# %%
import numpy as np
from utils import meta, io, folders
import os
import pyfar as pf
import json

# %%
sciebo_data_path = folders.sciebo_data_path()
files_dir = os.path.join(sciebo_data_path, 'literatur.json')
with open(files_dir, 'r') as fcc_file:
    s = json.load(fcc_file)

# %%
ahe = meta.Person(name='ahe', given_name='Anne', family_name='Heimes')

for key in s:
    data = s[key]
    data = np.array(data)

    # remove duplicated and decresinf f data
    mask = np.append([False], np.diff(data[:, 0]) <= 0)
    frequencies = np.delete(data[:, 0], np.where(mask))
    freq = np.delete(data[:, 1], np.where(mask))

    # create data object
    s_rand = pf.FrequencyData(freq, frequencies)

    # create meta data
    if 'reduced' in key:  # small-scale
        sample = meta.Sample(
            name='sine', surf_height_m=0.0204,
            surf_wavelength_m=0.0468)
        env = meta.Environment(name='Literatur', sample=sample)
    else:  # 3m scale
        sample = meta.Sample(
            name='sine_3m', diameter_m=3, surf_wavelength_m=0.117,
            surf_height_m=0.051)
        env = meta.Environment(name='Literatur_3m', sample=sample)
    scattering_rand_meta = meta.ScatteringCoefficientRandom(
        name=key, creater=ahe, environment=env,
        frequencies=s_rand.frequencies)

    # write data
    file_path_s_rand = io.write_comsol_set_with_meta(
        s_rand, scattering_rand_meta)
    print(file_path_s_rand)

# %%
