# %%
import pyfar as pf
import os
from utils import folders, io
import matplotlib.pyplot as plt
import numpy as np

# %%
%matplotlib qt
s_type = 'ScatteringCoefficientRandom'
folder = os.path.join(
    folders.sciebo_data_path(), 'scatpy', s_type)
files = os.listdir(folder)
plot_folder = os.path.join(
    folders.sciebo_data_path(), 'plots', 'auto', 'scattering_coefficients')
if not os.path.exists(plot_folder):
    os.mkdir(plot_folder)

fig, ax = plt.subplots()
for file in files:
    if not file.endswith('far'):
        continue
    s_rand, meta_s_rand = io.read_comsol_set_with_meta(
        os.path.join(folder, file))
    scale = meta_s_rand.environment.sample.scale_factor
    print(scale)
    s_rand_ = pf.FrequencyData(s_rand.freq, s_rand.frequencies/scale)

    pf.plot.freq(s_rand_, dB=False, label=meta_s_rand.name, ax=ax)
    plt.legend()
plt.savefig(os.path.join(plot_folder, f'{s_type}_all.png'))
plt.close()
# %%
s_type = 'ScatteringCoefficient'
folder = os.path.join(
    folders.sciebo_data_path(), 'scatpy', s_type)
files = os.listdir(folder)

for file in files:
    if not file.endswith('far'):
        continue
    s_rand, meta_s_rand = io.read_comsol_set_with_meta(
        os.path.join(folder, file))
    scale = meta_s_rand.environment.sample.scale_factor
    s_rand_ = pf.FrequencyData(s_rand.freq, s_rand.frequencies/scale)

    fig, ax = plt.subplots()
    pf.plot.freq(s_rand_, dB=False, label=meta_s_rand.name, ax=ax)
    # plt.legend()
    plt.savefig(os.path.join(plot_folder, f'{meta_s_rand.name}{s_type}_all.png'))
    plt.close()

# %%
for file in files:
    if not file.endswith('far'):
        continue
    s_rand, meta_s_rand = io.read_comsol_set_with_meta(
        os.path.join(folder, file))
    scale = meta_s_rand.environment.sample.scale_factor
    s_mean = np.average(s_rand.freq, 0)
    s_rand_ = pf.FrequencyData(s_mean, s_rand.frequencies/scale)
    thetas = np.round(
        meta_s_rand.incident_coords[1, :].get_sph(unit='deg')[..., 1])
    fig, ax = plt.subplots()
    pf.plot.freq(s_rand_, dB=False, label=thetas, ax=ax)
    plt.legend()
    plt.savefig(os.path.join(plot_folder, f'{meta_s_rand.name}{s_type}_theta.png'))
    plt.close()

# %%
