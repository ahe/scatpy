# %%
import imkar as ik
import pyfar as pf
from utils import io, folders
import matplotlib.pyplot as plt
import os

# %%
%matplotlib qt
model_names = ['point_120', 'point_230','point_1000', 'plane']
#model_names = ['point_120', 'point_230','point_1000', 'plane']
r_mics = [1, 100]
r_mics = [100]

heights = [0.03, 0.0402, 0.0504]
heights = [0.0402]
for model_name in model_names:
    for r_mic in r_mics:
        for height in heights:
            path = os.path.join(
                folders.data_path(), 'ScatteringCoefficient',
                f'{model_name}_{r_mic}_{height}.ScatteringCoefficient.far')

            s, meta_pattern = io.read_comsol_set_with_meta(path)
            s_rand = ik.scattering.coefficient.random_incidence(
                s, meta_pattern.incident_coords)
            # %matplotlib widget
            pf.plot.freq(s_rand, False, label=f'{model_name} r {r_mic} h {height}')
            print(f'Plot {model_name} r {r_mic}')
# plt.legend()
plt.show()

# %%
%matplotlib qt
ax = pf.plot.freq(s_rand, dB=False)
ax.set_ylim(0, 1)
plt.show()
