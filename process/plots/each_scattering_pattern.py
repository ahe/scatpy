# %%
import os
from utils import folders, io, plot
import matplotlib.pyplot as plt

# %%
%matplotlib qt
s_type = 'ScatteringPattern'
folder = os.path.join(
    folders.sciebo_data_path(), 'scatpy', s_type)
files = os.listdir(folder)
plot_folder = os.path.join(
    folders.sciebo_data_path(), 'plots', 'auto', 'Scattering_pattern')
if not os.path.exists(plot_folder):
    os.mkdir(plot_folder)

for file in files[:4]:
    if not file.endswith('far') or 'ref' in file:
        continue
    data, data_meta = io.read_comsol_set_with_meta(
        os.path.join(folder, file))
    split = file.split('.')
    split[0] += '_ref'

    data_ref, data_meta_ref = io.read_comsol_set_with_meta(
        os.path.join(folder, '.'.join(split)))

    thetas = data_meta.incident_coords.get_sph(unit='deg')[1, :, 1]
    phis = data_meta.incident_coords.get_sph(unit='deg')[:, 1, 0]
    frequencies = data.frequencies
    for theta in thetas:
        for phi in phis:
            for frequency in frequencies:
                plot.scattering_polar(
                    data, data_ref, data_meta.incident_coords,
                    data_meta.mics_coords, phi, theta, frequency)
                plt.savefig(os.path.join(
                    plot_folder,
                    f'{data_meta.name}_{frequency}_th_{theta}_phi_{phi}.png'))
                plt.close()


#plt.savefig(os.path.join(plot_folder, f'{s_type}_all.png'))
#plt.close()
# %%
