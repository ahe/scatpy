# %%
import pyfar as pf
import os
from utils import folders, io
import matplotlib.pyplot as plt

# %%
%matplotlib qt
folder = os.path.join(folders.sciebo_data_path(), 'scatpy', 'ScatteringCoefficientRandom')

files = [
    ('bem_mommertz.ScatteringCoefficientRandom.far', 'BEM Mommertz', 0.117),
    ('analytical.ScatteringCoefficientRandom.far', 'Analytical', 0.117),
    ('real_scale_measurement.ScatteringCoefficientRandom.far', 'real-scale measurement', 0.117),
    ('reduced_scale_measurement.ScatteringCoefficientRandom.far', 'reduced-scale measurement', 0.0468),
    # ('mesh2hrtf_sine_sample_10k.ScatteringCoefficientRandom.far', 'mesh2hrtf'),
    # ('point_120_100.ScatteringCoefficientRandom.far', 'Comsol 100'),
    # ('point_230_100.ScatteringCoefficientRandom.far', 'Comsol plane'),
    # ('point_1000_100.ScatteringCoefficientRandom.far', 'comsol 10m'),
    # ('plane_100.ScatteringCoefficientRandom.far', 'comsol 1'),
    ('mesh2hrtf_sine.ScatteringCoefficientRandom.far', 'mesh2hrtf', 0.0468),
    # ('mesh2hrtf_ita_100k.ScatteringCoefficientRandom.far', 'ita old'),
    # ('mesh2hrtf_ita.ScatteringCoefficientRandom.far', 'ita neu', 100),
    # ('mesh2hrtf_01_kunsthaus_zuerich.ScatteringCoefficientRandom.far', 'Kunsthaus Zürich', 20)
    ]
# files = [('mesh2hrtf_ita.ScatteringCoefficientRandom.far', 'ihta')]

for file in files:
    path = os.path.join(folder, file[0])
    s_rand, meta_s_rand = io.read_comsol_set_with_meta(path)
    lambda_f = 343 / s_rand.frequencies
    lambda_surf = meta_s_rand.environment.sample.surf_wavelength_m
    #lambda_surf = 0.117
    s_rand_norm = pf.FrequencyData(s_rand.freq, s_rand.frequencies*file[2]/0.117)
    print(s_rand_norm.frequencies)
    print(s_rand_norm.freq)
    #plt.semilogx(s_rand_norm.frequencies, s_rand.freq.flatten(), label = file[1])
    pf.plot.freq(s_rand_norm, dB=False, label = file[1])

plt.legend()
plt.grid(True)
plt.show()

#plt.axes().set_xlabels(['0.0625', '0.125', '0.25', '0.5', '1', '2', '4', '8'])

# %%
s_rand.freq
s_rand.frequencies

# %%
