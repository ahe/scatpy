# %%
from utils import spatial, m2s, meta, io, folders
import numpy as np
import pyfar as pf
import imkar as ik
import matplotlib.pyplot as plt
import os

# %%
# Set folder names
project_folder = '/home/anne/sciebo/Lehre/Seminar/22WS-mesh2hrtf/data'

# %%
# read data from mesh2hrtf
sample = 'sample'
sample_ref = 'reference'
simulation_name = f'mesh2hrtf_{project_folder}'
path = os.path.join(project_folder, 'sine', sample)
data, mic_coords, inc_coords, params = m2s.read_numcalc(path)

simulation_name_ref = f'mesh2hrtf_{project_folder}_ref'
path = os.path.join(project_folder, 'sine', sample_ref)
data_ref, mic_coords_ref, inc_coords_ref, params_ref = m2s.read_numcalc(
    path)

# %%

# reshape data for scattering and
radius = 1.5
all_az = np.linspace(0, 2*np.pi, 73)
all_az[-1] = 0
all_el = np.linspace(0, np.pi/2, 19)
mic_coords_out = pf.Coordinates(
    *np.meshgrid(all_az, all_el, indexing='ij'),
    radius, 'sph', 'top_colat')

data = spatial.reshape_to_az_by_el(data, mic_coords, mic_coords_out, 1)
mic_coords = mic_coords_out.copy()
data_ref = spatial.reshape_to_az_by_el(
    data_ref, mic_coords_ref, mic_coords_out, 1)
mic_coords_ref = mic_coords_out.copy()

# %%
# calculate scattering coefficient
s = ik.scattering.coefficient.freefield(
    data, data_ref, mic_coords)

# %%
# Plot scattering
%matplotlib qt
pf.plot.freq(s, dB=False)
plt.show()

# %%
# plot polar plot
%matplotlib qt
phi = 0
theta = 30
index_freq = 5
phi_mesh = mic_coords.get_sph()[..., 0]
theta_mesh = mic_coords.get_sph()[..., 1]
data_polar = data[0, :, :].freq[..., index_freq].copy()
data_polar_ref = data_ref[0, :, :].freq[..., index_freq].copy()

fig, ax = plt.subplots(1, 2, subplot_kw=dict(projection='polar'))
ax[0].contourf(
    phi_mesh, theta_mesh*180/np.pi, np.abs(data_polar),
    100, cmap='jet')
ax[1].contourf(
    phi_mesh, theta_mesh*180/np.pi, np.abs(data_polar_ref),
    100, cmap='jet')
ax[0].plot(phi*np.pi/180, theta, 'ro')
ax[0].set_title(f'sample\nf = {data.frequencies[index_freq]} Hz')
ax[1].plot(phi*np.pi/180, theta, 'ro')
ax[1].set_title(f'reference\nf = {data.frequencies[index_freq]} Hz', )
plt.show()

# %%

ref_s = pf.FrequencyData(
    [
        0.00151911, 0.00504863, 0.00979892, 0.04602728,
        0.61063135, 0.87200259, 0.93474583],
    [1250., 1600., 2000., 2500., 3150., 4000., 5000.])

# %%
%matplotlib qt
fig, ax = plt.subplots()
pf.plot.freq(s, dB=False, label= 'simulated')
pf.plot.freq(ref_s, dB=False, label='reference')
plt.legend()
plt.show()

# %%
