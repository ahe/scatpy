# Install

To create the environment run:

```
conda env create -f environment.yml --prefix ./env/scatpy/
```

and activate using:

```
conda activate ./env/scatpy
```

The specific environment (potentially very OS specific) can be exported

```
conda env export --prefix ./env/scatpy --file exact_environment.yml
```

and updated:

```
conda env update --prefix ./env/scatpy --file exact_environment.yml  --prune
```

update local packages based on environment.yml:
```
conda env update --prefix ./env/scatpy --file environment.yml  --prune
```